## Edwin-ML



Taller


# Jorge-repo

Repo para probar roles
## Indice
1. [Guest](#permisos-guest)
2. [Reporter](#permisos-reporter)
3. [Developer](#permisos-developer)
4. [Maintainer](#permisos-maintainer)

## Permisos Guest
- Se puede crear Issuses pero solo con un nombre y un comentario
- Permite cerrar Issues y pocas opciones más.
- No se permite crear labels
- No se permite crear milestones.
- No permite hacer push. Se muestra un mensaje.
- No se permite hacer commits.
- No se permite crear nuevas branches. No aparece la opción.

## Permisos Reporter 
- Se pueden crear Issues, ahora con Milestones, labels y weights, pero no se puede crear milestones.
- Se puede crar labels.
- Se puede modificar los issues, con varias opciones.
- No permite crear branches.
- no permite hacer push.
- No permite hacer commits.

## Permisos Developer
- Se pueden crear Issues, con todas las opciones
- Se puede crear milestones.
- Se puede crear labels.
- Permite hacer commits excepto en el master
- Permite hacer push.
- Permite crear ramas.
- No permite modificar nada del master.
- Permite hacer merge de ramas que no sean la master.

## Permisos Maintainer
- Se pueden crear Issues, con todas las opciones
- Se puede crear milestones.
- Se puede crear labels.
- Se puede hacer commit incluso en el master.
- Permite hacer push.
- Permite crear ramas.
- Permite hacer merge incluso con la master.




